﻿using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(UIButton))]
[CanEditMultipleObjects]
public class ButtonEditor : Editor {
    private SerializedProperty default_color;
    private SerializedProperty button_type;
    private SerializedProperty enter_color;
    private SerializedProperty pressed_color;
    private SerializedProperty disabled_color;
    private SerializedProperty is_toggle;
    private SerializedProperty disabled;
    private SerializedProperty toggle_color;
    private SerializedProperty tab_group;
    private SerializedProperty target_page;
    private SerializedProperty on_click;
 

    private void OnEnable(){
        default_color = serializedObject.FindProperty("default_color");
        button_type = serializedObject.FindProperty("type");
        enter_color = serializedObject.FindProperty("enter_color");
        pressed_color = serializedObject.FindProperty("pressed_color");
        disabled_color = serializedObject.FindProperty("disabled_color");
        is_toggle = serializedObject.FindProperty("is_toggle");
        disabled = serializedObject.FindProperty("disabled");
        toggle_color = serializedObject.FindProperty("toggle_color");
        tab_group = serializedObject.FindProperty("tab_group");
        target_page = serializedObject.FindProperty("target_page");
        on_click = serializedObject.FindProperty("on_click");
    }
 

    public override void OnInspectorGUI(){
        serializedObject.Update();
        UIButton button = (UIButton)target;

        EditorGUILayout.PropertyField(default_color);
        EditorGUILayout.PropertyField(button_type);
        if(button.type == UIButton.Type.Button){
            EditorGUI.indentLevel++;
            EditorGUILayout.PropertyField(enter_color);
            EditorGUILayout.PropertyField(pressed_color);
            EditorGUILayout.PropertyField(is_toggle);
            if(button.is_toggle){
                EditorGUI.indentLevel++;
                EditorGUILayout.PropertyField(toggle_color);
                EditorGUI.indentLevel--;
            }
            EditorGUILayout.PropertyField(disabled);
            if(button.disabled){
                EditorGUI.indentLevel++;
                EditorGUILayout.PropertyField(disabled_color);
                EditorGUI.indentLevel--;
            }
            EditorGUI.indentLevel--;
        }
        if(button.type == UIButton.Type.Tab){
            EditorGUI.indentLevel++;
            EditorGUILayout.PropertyField(tab_group);
            EditorGUILayout.PropertyField(target_page);
            EditorGUI.indentLevel--;
            button.is_toggle = false;
        }

        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(on_click, true);

        serializedObject.ApplyModifiedProperties();
    }
}
