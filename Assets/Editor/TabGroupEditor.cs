﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(UITabGroup))]
[CanEditMultipleObjects]
public class TabGroupEditor : Editor {


    public override void OnInspectorGUI(){
        base.OnInspectorGUI();
        serializedObject.Update();
        UITabGroup tab_group = (UITabGroup)target;

        if(GUILayout.Button("Generate Tab")){
            UIButton prefab = Instantiate(tab_group.button_prefab);
            prefab.transform.parent = tab_group.transform;
            prefab.type = UIButton.Type.Tab;
            prefab.tab_group = tab_group;
        }

        serializedObject.ApplyModifiedProperties();
    }
}
