﻿using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

[CustomEditor(typeof(GridLayout))]
[CanEditMultipleObjects]
public class GridEditor : Editor {
    private SerializedProperty button_prefab;
    private SerializedProperty grid_prefab;

    private SerializedProperty type;
    private SerializedProperty scroll_type;
    private SerializedProperty horizontal_scroll_clamp;
    private SerializedProperty vertical_scroll_clamp;

    private SerializedProperty rows;
    private SerializedProperty columns;
    
    private SerializedProperty spacing_x;
    private SerializedProperty spacing_y;
    private SerializedProperty padding_left;
    private SerializedProperty padding_right;
    private SerializedProperty padding_top;
    private SerializedProperty padding_bottom;

    private SerializedProperty visable_buttons;
    private SerializedProperty file_name;
    
    private bool dropdown_spacing = false;
    private bool dropdown_padding = false;
    private bool dropdown_generated = false;

    private void OnEnable(){
        button_prefab = serializedObject.FindProperty("button_prefab");
        grid_prefab = serializedObject.FindProperty("grid_prefab");

        type = serializedObject.FindProperty("type");
        scroll_type = serializedObject.FindProperty("scroll_type");
        horizontal_scroll_clamp = serializedObject.FindProperty("horizontal_scroll_clamp");
        vertical_scroll_clamp = serializedObject.FindProperty("vertical_scroll_clamp");

        rows = serializedObject.FindProperty("rows");
        columns = serializedObject.FindProperty("columns");

        spacing_x = serializedObject.FindProperty("spacing_x");
        spacing_y = serializedObject.FindProperty("spacing_y");
        padding_left = serializedObject.FindProperty("padding_left");
        padding_right = serializedObject.FindProperty("padding_right");
        padding_top = serializedObject.FindProperty("padding_top");
        padding_bottom = serializedObject.FindProperty("padding_bottom");

        visable_buttons = serializedObject.FindProperty("visable_buttons");
        file_name = serializedObject.FindProperty("file_name");
    }

    public override void OnInspectorGUI(){
        serializedObject.Update();
        GridLayout grid = (GridLayout)target;

        dropdown_padding = EditorGUILayout.Foldout(dropdown_padding, "Padding");
        if(dropdown_padding){
            EditorGUI.indentLevel++;
            EditorGUILayout.PropertyField(padding_left, new GUIContent("Left"));
            EditorGUILayout.PropertyField(padding_right, new GUIContent("Right"));
            EditorGUILayout.PropertyField(padding_top, new GUIContent("Top"));
            EditorGUILayout.PropertyField(padding_bottom, new GUIContent("Bottom"));
            EditorGUI.indentLevel--;
        }
        dropdown_spacing = EditorGUILayout.Foldout(dropdown_spacing, "Spacing");
        if(dropdown_spacing){
            EditorGUI.indentLevel++;
            EditorGUILayout.PropertyField(spacing_x, new GUIContent("X"));
            EditorGUILayout.PropertyField(spacing_y, new GUIContent("Y"));
            EditorGUI.indentLevel--;
            EditorGUILayout.Space();
        }

        EditorGUILayout.PropertyField(button_prefab);
        EditorGUILayout.PropertyField(grid_prefab);

        EditorGUILayout.PropertyField(type);
        EditorGUI.indentLevel++;

        if(grid.type == GridLayout.Type.Carousel){
            grid.viewport.GetComponent<Image>().enabled = true;
            grid.viewport.GetComponent<Mask>().enabled = true;
            grid.viewport.GetComponent<ScrollRect>().enabled = true;

            EditorGUILayout.PropertyField(scroll_type);
            EditorGUILayout.PropertyField(visable_buttons);

            if(grid.scroll_type == GridLayout.ScrollType.Horizontal){
                grid.viewport.GetComponent<ScrollRect>().horizontal = true;
                grid.viewport.GetComponent<ScrollRect>().vertical = false;
                EditorGUILayout.PropertyField(rows);
                EditorGUILayout.PropertyField(horizontal_scroll_clamp);
            }
            if(grid.scroll_type == GridLayout.ScrollType.Vertical){
                grid.viewport.GetComponent<ScrollRect>().horizontal = false;
                grid.viewport.GetComponent<ScrollRect>().vertical = true;
                EditorGUILayout.PropertyField(columns);
                EditorGUILayout.PropertyField(vertical_scroll_clamp);
            }
        }
        else{
            EditorGUILayout.PropertyField(rows);
            EditorGUILayout.PropertyField(columns);
            grid.viewport.GetComponent<Image>().enabled = false;
            grid.viewport.GetComponent<Mask>().enabled = false;
            grid.viewport.GetComponent<ScrollRect>().enabled = false;
        }
        EditorGUI.indentLevel--;

        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        if(GUILayout.Button("Generate Button")){
            UIButton prefab = Instantiate(grid.button_prefab);
            prefab.transform.parent = grid.transform;
            prefab.type = UIButton.Type.Button;
            prefab.name = "Button";
        }
        if(GUILayout.Button("Generate Grid")){
            GameObject temp = Instantiate(new GameObject());
            temp.transform.parent = grid.transform;
            temp.name = "Grid";
            temp.AddComponent<GridLayout>();
            temp.GetComponent<GridLayout>().button_prefab = grid.button_prefab;
            temp.GetComponent<GridLayout>().grid_prefab = grid.grid_prefab;
        }
        if(GUILayout.Button("Clear All")){
            while(grid.transform.childCount > 0){
                foreach (Transform child in grid.transform) {
                    GameObject.DestroyImmediate(child.gameObject);
                }
            }
        }

        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = 60;
        EditorGUILayout.PropertyField(file_name);
        if(GUILayout.Button("Re-Generate JSON", GUILayout.Width(175), GUILayout.Height(20))){
            grid.generate_from_json();
        }
        GUILayout.EndHorizontal();

        serializedObject.ApplyModifiedProperties();
    }
}
