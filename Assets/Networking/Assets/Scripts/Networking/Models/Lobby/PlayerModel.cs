﻿using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PlayerModel {
    public string uuid;
    public string nickname;
    public bool ready;
    public bool host;
}
