﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ServerCommunication : MonoBehaviour {
    [SerializeField]
    private string hostIP;

    [SerializeField]
    private int port = 3000;

    [SerializeField]
    private bool useLocalhost = true;

    private string host => useLocalhost ? "localhost" : hostIP;
    private string server;
    private WsClient client;

    public LobbyMessaging Lobby { private set; get; }

    /// Unity method called on initialization
    private void Awake() {
        server = "ws://" + host + ":" + port;
        client = new WsClient(server);
        Lobby = new LobbyMessaging(this);
    }

    /// Unity method called every frame
    private void Update() {
        // Check if server send new messages
        var cqueue = client.receiveQueue;
        string msg;
        while (cqueue.TryPeek(out msg))
        {
            // Parse newly received messages
            cqueue.TryDequeue(out msg);
            HandleMessage(msg);
        }
    }

    private void HandleMessage(string msg) {
        Debug.Log("Server: " + msg);

        // Deserializing message from the server
        var message = JsonUtility.FromJson<MessageModel>(msg);

        // Picking correct method for message handling
        // TODO: Revise these for our own protocol, store in a better place
        switch (message.method) {
            case LobbyMessaging.Register:
                Lobby.OnConnectedToServer?.Invoke();
                break;
            case LobbyMessaging.ChangeNicknameMethod:
                Lobby.OnChangeNickname?.Invoke(
                    JsonUtility.FromJson<PlayerModel>(msg)
                );
                break;
            case LobbyMessaging.SetHostMethod:
                Lobby.OnSetHost?.Invoke(
                    JsonUtility.FromJson<HostModel>(msg)
                );
                break;
            case LobbyMessaging.ToggleReadyMethod:
                Lobby.OnToggleReady?.Invoke(
                    JsonUtility.FromJson<ReadyStatusModel>(msg)
                );
                break;
            case LobbyMessaging.AddPlayerMethodName:
                // TODO show UUID of player joining in debug message
                Debug.Log("[Server] Player added: ...");

                Lobby.OnAddPlayer?.Invoke(
                    JsonUtility.FromJson<PlayerModel>(msg)
                );
                break;
            case LobbyMessaging.RemovePlayerMethodName:
                // TODO show UUID of player leaving in debug message
                Debug.Log("[Server] Player disconnected: ...");

                Lobby.OnRemovePlayer?.Invoke(
                    JsonUtility.FromJson<PlayerModel>(msg)
                );
                break;
            default:
                Debug.LogError("Unknown type of method: " + message.method);
                break;
        }
    }

    public async void ConnectToServer() {
        await client.Connect();
    }

    public void SendRequest(string message) {
        client.Send(message);
    }
}
