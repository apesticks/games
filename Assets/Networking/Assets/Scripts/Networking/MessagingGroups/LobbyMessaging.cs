﻿using UnityEngine;
using UnityEngine.Events;

public class LobbyMessaging : BaseMessaging {
    public LobbyMessaging(ServerCommunication client) : base(client) { }

    // TODO: Find a better place to store these commands
    public const string Register = "register";
    public UnityAction OnConnectedToServer;

    public const string SetHostMethod = "setHost";
    public UnityAction<HostModel> OnSetHost;

    public const string ChangeNicknameMethod = "changeNickname";
    public UnityAction<PlayerModel> OnChangeNickname;

    public const string ToggleReadyMethod = "toggleReady";
    public UnityAction<ReadyStatusModel> OnToggleReady;

    public const string AddPlayerMethodName = "addPlayer";
    public UnityAction<PlayerModel> OnAddPlayer;

    public const string RemovePlayerMethodName = "removePlayer";
    public UnityAction<PlayerModel> OnRemovePlayer;

    public void ChangeNickname(EchoMessageModel request) {
        var message = new MessageModel {
            method = "changeNickname",
            message = JsonUtility.ToJson(request)
        };
        client.SendRequest(JsonUtility.ToJson(message));
    }

    public void ToggleReady(EchoMessageModel request) {
        var message = new MessageModel {
            method = "toggleReady",
        };
        client.SendRequest(JsonUtility.ToJson(message));
    }
}
