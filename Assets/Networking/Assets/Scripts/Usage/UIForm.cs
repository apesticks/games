﻿using UnityEngine;
using UnityEngine.Events;
using TMPro;

// This class should be assigned to a GameObject.
//
// To bind a new action, assign that GameObject to the On_click() event of
// another GameObject from the inspector (e.g. a Button), and set that
// action's script to the GameObject where this script was assigned.
public class UIForm : MonoBehaviour
{
    [SerializeField]
    private TMP_InputField inputField;

    [SerializeField]
    private TextMeshProUGUI serverResponse;

    public string InputFieldText => inputField.text;
    public UnityAction OnSendButtonClicked;
    public UnityAction OnReadyButtonClicked;

    public void ClearInputField() {
        inputField.text = "";
    }

    public void SendButtonClicked() {
        OnSendButtonClicked?.Invoke();
    }

    public void ReadyButtonClicked() {
        OnReadyButtonClicked?.Invoke();
    }

    public void ShowServerResponse(string message) {
        // TODO: Make this more appropriate to our use case
        serverResponse.text = $"Server Response:\n{message}";
    }
}
