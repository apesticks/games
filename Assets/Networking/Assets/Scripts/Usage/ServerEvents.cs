﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


// This class defines which Unity Actions will respond when server methods
// are executed. They are bound to events in ServerEvents, which also passes
// through any needed context.
public class ServerEvents : MonoBehaviour {

    //[SerializeField]
    private ServerCommunication communication;
    public GameObject row_prefab;
    public GameObject parent;

    //[SerializeField]
    //public UIForm uiForm;
    private UIForm uiForm;
    //
    //[SerializeField]
    //public UIForm readyUIForm;

    public GameObject lobby;
    private Dictionary<string, GameObject> playerRows;

    // TODO: Move this list to somewhere else that makes more sense?
    public List<PlayerModel> players;

    private void Start() {
        uiForm = lobby.GetComponent<UIForm>();
        communication = GetComponent<ServerCommunication>();

        communication.Lobby.OnConnectedToServer += OnConnectedToServer;
        communication.ConnectToServer();

        playerRows = new Dictionary<string, GameObject>();
    }

    private void OnConnectedToServer() {
        // TODO: Automatically bind these to the matching method?

        // Binding callbacks to unity actions for websocket-triggered actions
        communication.Lobby.OnConnectedToServer -= OnConnectedToServer;
        communication.Lobby.OnChangeNickname += OnChangeNickname;
        communication.Lobby.OnSetHost += OnSetHost;
        communication.Lobby.OnAddPlayer += OnAddPlayer;
        communication.Lobby.OnRemovePlayer += OnRemovePlayer;
        communication.Lobby.OnToggleReady += OnToggleReady;

        uiForm.OnSendButtonClicked += OnSendForm;
        uiForm.OnReadyButtonClicked += OnSendToggleReady;
    }

    private void OnAddPlayer(PlayerModel player) {
        Debug.Log("[Server] New player joined: [" + player.uuid + "]");
        players.Add(player);
        AddPlayerRow(player);
    }

    public void AddPlayerRow(PlayerModel player) {
        GameObject prefab = Instantiate(row_prefab);
        prefab.transform.parent = parent.transform;
        SetPlayerRow(player, prefab);

        playerRows.Add(player.uuid, prefab);
    }

    public void SetPlayerRow(PlayerModel player, GameObject row) {
        string name;

        if (player.nickname != "") {
            name = player.nickname;
        } else {
            name = player.uuid;
        }

        if (player.host) {
        }

        row.GetComponentInChildren<Text>().text = name;
    }

    public void RemovePlayerRow(string uuid) {
        Destroy(playerRows[uuid]);
        playerRows.Remove(uuid);
    }

    private void OnRemovePlayer(PlayerModel player) {
        // TODO: Add handler for non-graceful disconnection
        Debug.Log("[Server] Player [" + player.uuid + "] has disconnected.");
        foreach (PlayerModel curPlayer in players) {
            if (curPlayer.uuid == player.uuid) {
                players.Remove(curPlayer);
                RemovePlayerRow(curPlayer.uuid);
                break;
            }
        }
    }

    private void OnSendForm() {
        //var player = GetPlayer();
        var message = new EchoMessageModel {
            text = uiForm.InputFieldText
            //playerName = player.name;
        };
        communication.Lobby.ChangeNickname(message);
        uiForm.ClearInputField();
    }

    private void OnSendToggleReady() {
        //var player = GetPlayer();
        var message = new EchoMessageModel {
            text = ""
        };
        communication.Lobby.ToggleReady(message);
    }

    private void OnSetHost(HostModel host) {
        Debug.Log("[Server] Host changed: " + host.uuid);

        foreach (PlayerModel curPlayer in players) {
            curPlayer.host = (host.uuid == curPlayer.uuid);
            /*
            if (curPlayer.uuid == player.uuid) {
                curPlayer.host = true;
            } else {
                curPlayer.host = false;
            }
            */
        }
        //uiForm.ShowServerResponse(message.text);
    }

    private void OnChangeNickname(PlayerModel player) {
        Debug.Log("[Server] Nickname changed (" + player.uuid + "): " + player.nickname);
        //string targetUUID = player.uuid;
        foreach (PlayerModel curPlayer in players) {
            if (curPlayer.uuid == player.uuid) {
                curPlayer.nickname = player.nickname;
                GameObject row = playerRows[player.uuid];
                SetPlayerRow(player, row);
                break;
            }
        }
        //uiForm.ShowServerResponse(message.text);
    }

    private void OnToggleReady(ReadyStatusModel status) {
        foreach (PlayerModel curPlayer in players) {
            if (curPlayer.uuid == status.uuid) {
                Debug.Log("[Server] Player toggled ready (" + curPlayer.uuid + "): " + curPlayer.nickname);
                curPlayer.ready = !curPlayer.ready;
            }
        }
        //uiForm.ShowServerResponse(message.text);
    }

    private void OnReceivedEchoMessage(EchoMessageModel message) {
        Debug.Log("Echo message received: " + message.text);
        uiForm.ShowServerResponse(message.text);
    }

    private void OnDisable() {
        communication.Lobby.OnConnectedToServer -= OnConnectedToServer;
        communication.Lobby.OnChangeNickname -= OnChangeNickname;
        communication.Lobby.OnSetHost -= OnSetHost;
        uiForm.OnReadyButtonClicked -= OnSendToggleReady;
        uiForm.OnSendButtonClicked -= OnSendForm;
    }
}
