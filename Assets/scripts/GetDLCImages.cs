﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetDLCImages : MonoBehaviour{

    public UITabGroup tab_group;
    private string selected_game;
    private bool found; 

    void Start(){
        JSONReader.load_json<Games>("games");
    }

    void Update(){
        if(tab_group.button_selected != null){
            found = false;
            Games games = JSONReader.get_json<Games>("games");
            foreach(Game game in games.games){
                foreach(DLC dlc in game.dlc){
                    if(dlc.name == tab_group.button_selected.name){
                        found = true;
                        if(selected_game != dlc.name){
                            selected_game = dlc.name;
                            foreach(Transform child in transform) {
                                GameObject.Destroy(child.gameObject);
                            }
                            for(int i=0; i<dlc.images.Count; i++){
                                GameObject temp = new GameObject();
                                temp.transform.parent = this.transform;
                                temp.AddComponent<Image>();
                                Sprite sprite = Resources.Load<Sprite>(dlc.images[i]);
                                temp.GetComponent<Image>().sprite = sprite;
                            }
                            return;
                        }
                    }
                }
            }
            if(!found){
                selected_game = null;
            }
        }
        else{
            if(transform.childCount > 0){
                foreach(Transform child in transform) {
                    GameObject.Destroy(child.gameObject);
                }
            }
        }
    }
}
