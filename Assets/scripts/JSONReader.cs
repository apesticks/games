﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;



public static class JSONReader{

    public static Dictionary<string, dynamic> dict = new Dictionary<string, dynamic>();

    public static T load_json<T>(string file_name){
        string path = Application.streamingAssetsPath + "/" + file_name + ".json";
        if(!System.IO.File.Exists(path)){
            throw new Exception("file " + path + " does not exist");
        }
        if(!dict.ContainsKey(file_name)){
            string data = File.ReadAllText(path);
            T json = JsonUtility.FromJson<T>(data);
            dict.Add(file_name, json);
        }
        return dict[file_name];
    }

    public static T update_json<T>(string file_name, T object_data){
        string path = Application.streamingAssetsPath + "/" + file_name + ".json";
        string string_data = JsonUtility.ToJson(object_data, true);
        File.WriteAllText(path, string_data);

        string data = File.ReadAllText(path);
        dict[file_name] = JsonUtility.FromJson<T>(data);

        return dict[file_name];
    }

    public static T get_json<T>(string file_name){
        if(dict.ContainsKey(file_name)){
            return dict[file_name];
        }
        else{
            throw new Exception("json does not exist in dict - must be loaded first using load_json(string)");
        }
    }
}


[System.Serializable]
public class DLC{
    public string name;
    public int id;
    public string description;
    public List<string> images;
    public bool purchased;
}


[System.Serializable]
public class Owned{
    public string name;
    public List<int> dlc;
}


[System.Serializable]
public class User{
    public string name;
    public int id;
    public List<Owned> owned;
}


[System.Serializable]
public class Game{
    public int id;
    public bool purchased;
    public string name;
    public string description;
    public string[] images;
    public List<DLC> dlc;
}


[System.Serializable]
public class Games{
    public Game[] games;
}
