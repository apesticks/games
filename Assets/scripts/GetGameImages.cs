﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetGameImages : MonoBehaviour{

    public string file_name;
    public UITabGroup tab_group;
    private string selected_game;
    private bool found; 

    void Start(){
        if(file_name == null){
            Debug.LogError("file_name cannot == null");
        }
        JSONReader.load_json<Games>(file_name);
    }

    void Update(){
        if(tab_group.button_selected != null){
            found = false;
            Games games = JSONReader.get_json<Games>(file_name);
            foreach(Game game in games.games){
                if(game.name == tab_group.button_selected.name){
                    found = true;
                    if(selected_game != game.name){
                        selected_game = game.name;
                        foreach(Transform child in transform) {
                            GameObject.Destroy(child.gameObject);
                        }
                        for(int i=0; i<game.images.Length; i++){
                            GameObject temp = new GameObject();
                            temp.transform.parent = this.transform;
                            temp.AddComponent<Image>();
                            Sprite sprite = Resources.Load<Sprite>(game.images[i]);
                            temp.GetComponent<Image>().sprite = sprite;
                        }
                        return;
                    }
                }
            }
            if(!found){
                selected_game = null;
            }
        }
    }
}
