﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurchaseDLC : MonoBehaviour {

    public UITabGroup tab_group;
    private UIButton button;

    void Start() {
        button = this.GetComponent<UIButton>();
        JSONReader.load_json<Games>("games");
    }

    void Update() {
        if(tab_group.button_selected != null){
            Games games = JSONReader.get_json<Games>("games");
            foreach(Game game in games.games){
                foreach(DLC dlc in game.dlc){
                    if(dlc.name == tab_group.button_selected.name){
                        User user = JSONReader.get_json<User>("user");
                        foreach(Owned user_game in user.owned){
                            if(user_game.name == game.name){
                                if(user_game.dlc.Contains(dlc.id)){
                                    button.disabled = true;
                                    this.name = "Purchased";
                                }
                                else{
                                    button.disabled = false;
                                    this.name = "Purchase";
                                }
                            }
                        }
                        //if(!dlc.purchased){
                        //    button.disabled = false;
                        //    this.name = "Purchase";
                        //}
                        //else{
                        //    button.disabled = true;
                        //    this.name = "Purchased";
                        //}
                    }
                }
            }
        }
    }

    public void purchase(){
        if(!button.disabled){
            if(tab_group.button_selected != null){
                Games games = JSONReader.get_json<Games>("games");
                foreach(Game game in games.games){
                    foreach(DLC dlc in game.dlc){
                        if(dlc.name == tab_group.button_selected.name){
                            dlc.purchased = true;
                            JSONReader.update_json("games", games);

                            User user = JSONReader.get_json<User>("user");
                            foreach(Owned user_game in user.owned){
                                if(user_game.name == game.name){
                                    user_game.dlc.Add(dlc.id);
                                    JSONReader.update_json("user", user);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
