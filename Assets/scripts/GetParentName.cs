﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


[ExecuteInEditMode]
public class GetParentName : MonoBehaviour{

    private TextMeshProUGUI TMP_text;
    private RectTransform rect;

    void Start(){
        TMP_text = this.GetComponent<TextMeshProUGUI>();
        TMP_text.text = transform.parent.name;

        rect = this.GetComponent<RectTransform>();
        rect.offsetMin = new Vector2(0, 0);
        rect.offsetMax = new Vector2(0, 0);
        rect.sizeDelta = new Vector2(0, 0);
        rect.anchoredPosition = new Vector2(0, 0);
    }

    void Update(){
        TMP_text = this.GetComponent<TextMeshProUGUI>();
        TMP_text.text = transform.parent.name;

        rect = this.GetComponent<RectTransform>();
        rect.offsetMin = new Vector2(0, 0);
        rect.offsetMax = new Vector2(0, 0);
        rect.sizeDelta = new Vector2(0, 0);
        rect.localPosition = new Vector2(0, 0);
        rect.anchoredPosition = new Vector2(0, 0);
    }
}
