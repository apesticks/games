﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class GetGameDescription : MonoBehaviour {

    public string file_name;
    public UITabGroup tab_group;
    private TextMeshProUGUI TMP_text;

    void Start(){
        if(file_name == null){
            Debug.LogError("file_name cannot == null");
        }
        JSONReader.load_json<Games>(file_name);
    }

    public void update_description(){
        TMP_text = this.GetComponent<TextMeshProUGUI>();

        Games games = JSONReader.get_json<Games>(file_name);
        foreach(Game game in games.games){
            if(game.name == tab_group.button_selected.name){
                TMP_text.text = game.description;
                return;
            }
        }
    }
}
