﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CanPlay : MonoBehaviour {

    public UITabGroup tab_group;
    public string file_name;
    private UIButton button;

    void Start() {
        button = this.GetComponent<UIButton>();
        JSONReader.load_json<Games>(file_name);
    }

    void Update() {
        if(tab_group.button_selected != null){
            Games games = JSONReader.get_json<Games>(file_name);
            foreach(Game game in games.games){
                if(game.name == tab_group.button_selected.name){
                    if(!game.purchased){
                        this.name = "Purchase";
                    }
                    else{
                        this.name = "Play";
                    }
                }
            }
        }
    }
}
