﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurchaseGame : MonoBehaviour{

    public UITabGroup tab_group;
    public string file_name;
    private UIButton button;

    void Start(){
        button = this.GetComponent<UIButton>();
        JSONReader.load_json<Games>(file_name);
        JSONReader.load_json<User>("user");
    }
        
    public void purchase(){
        if(button.name == "Purchase"){
            if(tab_group.button_selected != null){
                Games games = JSONReader.get_json<Games>(file_name);
                foreach(Game game in games.games){
                    if(game.name == tab_group.button_selected.name){
                        game.purchased = true;
                        JSONReader.update_json(file_name, games);

                        User user = JSONReader.get_json<User>("user");
                        bool found = false;
                        foreach(Owned user_game in user.owned){
                            if(user_game.name == game.name){
                                found = true;
                                break;
                            }
                        }
                        if(!found){
                            Owned temp = new Owned();
                            temp.name = game.name;
                            temp.dlc = new List<int>();
                            user.owned.Add(temp);
                            JSONReader.update_json("user", user);
                        }
                    }
                }
            }
        }
    }
}
