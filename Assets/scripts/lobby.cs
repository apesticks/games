﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameSocket {
    private bool players_loaded = false;

    public GameSocket(string destination) {
        // TODO: Connect to actual server
    }

    public List<string> get_players() {
        // TODO: Support avatar images
        List<string> players = new List<string>();

        // TODO: Populate with actual values from server
        players.Add("bob");
        players.Add("nancy");
        players.Add("clayton");
        players.Add("regis");
        players.Add("martha");

        players_loaded = true;
        return players;
    }
}

public class lobby : MonoBehaviour {
    public string destination;
    public GameObject loading_message;
    public GameObject player_list;

    private GameSocket socket;
    private List<string> players;

    void Start() {
        socket = new GameSocket(this.destination);
        players = socket.get_players();
        //loading_message.SetActive(false);

        set_players(players);
    }
    public void set_players(List<string> players) {
        string names = "";

        foreach (string name in players) {
            names += name + "\n";
        }

        // TODO: Individual player text objects
        //player_list.GetComponent<Text>().text = names;
    }

    public void navigate_to_menu() {
        SceneManager.LoadScene("Menu");
    }
}
