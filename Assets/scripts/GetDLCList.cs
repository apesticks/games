﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetDLCList : MonoBehaviour {

    public UITabGroup tab_group;
    private string selected_game;
    private bool found; 

    void Start(){
        JSONReader.load_json<Games>("games");
    }

    void Update(){
        if(tab_group.button_selected != null){
            found = false;
            Games games = JSONReader.get_json<Games>("games");
            foreach(Game game in games.games){
                if(game.name == tab_group.button_selected.name){
                    found = true;
                    if(selected_game != game.name){
                        selected_game = game.name;
                        foreach(Transform child in transform) {
                            GameObject.Destroy(child.gameObject);
                        }
                        for(int i=0; i<game.dlc.Count; i++){
                            GridLayout grid = this.GetComponent<GridLayout>();
                            UIButton prefab = Instantiate(grid.button_prefab);
                            prefab.transform.parent = this.transform;
                            prefab.type = UIButton.Type.Tab;
                            prefab.name = game.dlc[i].name;
                            prefab.tab_group = this.GetComponent<UITabGroup>();
                        }
                        return;
                    }
                }
            }
            if(!found){
                selected_game = null;
            }
        }
    }
}
