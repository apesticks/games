﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class main_menu : MonoBehaviour {

    public GameObject button;
    public GameObject canvas;
    public GameObject menu_parent;
    public GameObject DLC_parent;
    public float x_offset_percent = 0.25f;
    public float y_offset_percent = 0.25f;
    public float tile_pixel_xoffset = 200;
    public float tile_pixel_yoffset = 50;
    public int row_count = 4;
    
    void Start(){

       // Vector2 canvas_corner = new Vector2(canvas.transform.position.x - canvas.GetComponent<RectTransform>().rect.width/2, canvas.transform.position.y + canvas.GetComponent<RectTransform>().rect.height/2);
       // Vector2 button_size = new Vector2(button.GetComponent<RectTransform>().rect.width, button.GetComponent<RectTransform>().rect.height);

       // float default_x = canvas_corner[0] + (canvas.GetComponent<RectTransform>().rect.width * x_offset_percent);
       // float start_x = canvas_corner[0] + (canvas.GetComponent<RectTransform>().rect.width * x_offset_percent);
       // float start_y = canvas_corner[1] - (canvas.GetComponent<RectTransform>().rect.height * y_offset_percent);

       // int counter = 0;

       // for(int i=0; i<12; i++){
       //     GameObject pref = Instantiate(button);
       //     pref.transform.parent = menu_parent.transform;
       //     pref.transform.position = new Vector3(start_x + button_size[0]/2, start_y - button_size[1]/2, 0);
       //     start_x = pref.transform.position.x + tile_pixel_xoffset;

       //     counter++;

       //     if(counter >= row_count){
       //         start_x = default_x;
       //         start_y = start_y - button_size[1] - tile_pixel_yoffset;
       //         counter = 0;
       //     }
       // }
    }

    public void play_button(){
        SceneManager.LoadScene("GameLobby");
    }

    public void quit_button(){
        Debug.Log("quit");
        Application.Quit();
    }

    public void options_button(){ 
        GameObject button_object = GameObject.FindGameObjectWithTag("video_button");
        //Button button = button_object.GetComponent<Button>();
        //button.Select();
    }
}
