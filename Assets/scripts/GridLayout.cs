﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[ExecuteInEditMode]
public class GridLayout : LayoutGroup{

    public enum Type{Normal, Fixed_rows, Fixed_columns, Carousel};
    public enum ScrollType{Horizontal, Vertical};
    public enum HorizontalScrollClamp{Left, Middle, Right};
    public enum VerticalScrollClamp{Top, Middle, Bottom};

    public UIButton button_prefab;
    public GridLayout grid_prefab;

    public Type type;
    public ScrollType scroll_type = ScrollType.Horizontal;
    public HorizontalScrollClamp horizontal_scroll_clamp = HorizontalScrollClamp.Middle;
    public VerticalScrollClamp vertical_scroll_clamp = VerticalScrollClamp.Middle;

    public int rows;
    public int columns;
    public float spacing_x;
    public float spacing_y;
    public int visable_buttons;
    public int padding_left;
    public int padding_right;
    public int padding_top;
    public int padding_bottom;
    public Vector2 cell_size;

    public GameObject viewport = null;
    public string file_name;

    private Dictionary<string, bool> done_once = new Dictionary<string, bool>(){
        {"viewport", false},
        {"no_viewport", false},
        {"anchor_h", false},
        {"anchor_v", false},
    };
    private Vector2 spacing;
    private bool viewport_created = false;

    void Start(){
        if(!Application.isPlaying && !transform.parent.name.Contains("viewport")){
            setup_viewport();

            this.GetComponent<RectTransform>().anchorMin = new Vector2(0, 0);
            this.GetComponent<RectTransform>().anchorMax = new Vector2(1, 1);
            this.transform.position = viewport.transform.position;
            this.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
            viewport_created = true;
        }
    }

    void setup_viewport(){
        viewport = new GameObject();
        viewport.name = "viewport";
        viewport.transform.parent = transform.parent;
        transform.parent = viewport.transform;
        viewport.AddComponent<Image>();
        viewport.AddComponent<Mask>();
        viewport.AddComponent<ScrollRect>();

        viewport.GetComponent<ScrollRect>().content = GetComponent<RectTransform>();
        viewport.GetComponent<ScrollRect>().viewport = viewport.GetComponent<RectTransform>();
        viewport.GetComponent<Mask>().showMaskGraphic = false;

        viewport.GetComponent<Image>().enabled = false;
        viewport.GetComponent<Mask>().enabled = false;
        viewport.GetComponent<ScrollRect>().enabled = false;

        viewport.GetComponent<RectTransform>().anchorMin = new Vector2(0, 0);
        viewport.GetComponent<RectTransform>().anchorMax = new Vector2(1, 1);
        viewport.transform.position = viewport.transform.parent.position;
        viewport.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
    }

    void do_the_stuff(){
        padding.left = padding_left;
        padding.right = padding_right;
        padding.top = padding_top;
        padding.bottom = padding_bottom;

        if(type == Type.Carousel){
            if(!done_once["viewport"]){
                done_once["viewport"] = true;
            }
            done_once["no_viewport"] = false;


            if(scroll_type == ScrollType.Horizontal){
                if(!done_once["anchor_h"]){
                    done_once["anchor_h"] = true;
                    done_once["anchor_v"] = false;
                    this.GetComponent<RectTransform>().anchorMin = new Vector2(0.5f, 0);
                    this.GetComponent<RectTransform>().anchorMax = new Vector2(0.5f, 1);
                    this.transform.position = viewport.transform.position;
                }
                float button_size = (viewport.GetComponent<RectTransform>().rect.width) / visable_buttons * rows;
                float width = (button_size * rectChildren.Count) / rows;
                GetComponent<RectTransform>().sizeDelta = new Vector2(width, 0);
                columns = Mathf.CeilToInt(transform.childCount / (float)rows);
                if(horizontal_scroll_clamp == HorizontalScrollClamp.Left){
                    Vector2 pivot = this.GetComponent<RectTransform>().pivot;
                    this.GetComponent<RectTransform>().pivot = new Vector2(0, pivot.y);
                }
                if(horizontal_scroll_clamp == HorizontalScrollClamp.Middle){
                    Vector2 pivot = this.GetComponent<RectTransform>().pivot;
                    this.GetComponent<RectTransform>().pivot = new Vector2(0.5f, pivot.y);
                }
                if(horizontal_scroll_clamp == HorizontalScrollClamp.Right){
                    Vector2 pivot = this.GetComponent<RectTransform>().pivot;
                    this.GetComponent<RectTransform>().pivot = new Vector2(1, pivot.y);
                }
            }
            if(scroll_type == ScrollType.Vertical){
                if(!done_once["anchor_v"]){
                    done_once["anchor_v"] = true;
                    done_once["anchor_h"] = false;
                    this.GetComponent<RectTransform>().anchorMin = new Vector2(0, 0.5f);
                    this.GetComponent<RectTransform>().anchorMax = new Vector2(1, 0.5f);
                    this.transform.position = viewport.transform.position;
                }
                float button_size = (viewport.GetComponent<RectTransform>().rect.height) / visable_buttons;
                float height = (button_size * rectChildren.Count);
                this.GetComponent<RectTransform>().sizeDelta = new Vector2(0, height);
                rows = Mathf.CeilToInt(transform.childCount / (float)columns);
                if(vertical_scroll_clamp == VerticalScrollClamp.Top){
                    Vector2 pivot = this.GetComponent<RectTransform>().pivot;
                    this.GetComponent<RectTransform>().pivot = new Vector2(pivot.x, 1);
                }
                if(vertical_scroll_clamp == VerticalScrollClamp.Middle){
                    Vector2 pivot = this.GetComponent<RectTransform>().pivot;
                    this.GetComponent<RectTransform>().pivot = new Vector2(pivot.x, 0.5f);
                }
                if(vertical_scroll_clamp == VerticalScrollClamp.Bottom){
                    Vector2 pivot = this.GetComponent<RectTransform>().pivot;
                    this.GetComponent<RectTransform>().pivot = new Vector2(pivot.x, 0);
                }
            }
        }
        else{
            if(!done_once["no_viewport"]){
                done_once["no_viewport"] = true;
                this.GetComponent<RectTransform>().anchorMin = new Vector2(0, 0);
                this.GetComponent<RectTransform>().anchorMax = new Vector2(1, 1);
                this.transform.position = viewport.transform.position;
                this.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
            }
            done_once["viewport"] = false;

            if(type == Type.Fixed_columns){
                rows = Mathf.CeilToInt(transform.childCount / (float)columns);
            }
            if(type == Type.Fixed_rows){
                columns = Mathf.CeilToInt(transform.childCount / (float)rows);
            }
            if(type == Type.Normal){
                rows = (int)Mathf.Round(Mathf.Sqrt(transform.childCount));
                if(rows == 0){ rows = 1; }
                columns = Mathf.CeilToInt(Mathf.Sqrt(transform.childCount));
                if(columns == 0){ columns = 1; }
            }
        }

        float parent_width = rectTransform.rect.width;
        float parent_height = rectTransform.rect.height;

        cell_size.x = (parent_width / (float)columns) - (spacing_x / columns * (columns-1)) - (padding.left / (float)columns) - (padding.right / (float)columns);
        cell_size.y = (parent_height / (float)rows) - (spacing_y / rows * (rows-1)) - (padding.top / (float)rows) - (padding.bottom / (float)rows);

        int row_count = 0;
        int column_count = 0;

        for(int i=0; i<rectChildren.Count; i++){
            row_count = i / columns;
            column_count = i % columns;

            var item = rectChildren[i];

            var x_pos = (cell_size.x * column_count) + (spacing_x * column_count) + padding.left;
            var y_pos = (cell_size.y * row_count) + (spacing_y * row_count) + padding.top;

            SetChildAlongAxis(item, 0, x_pos, cell_size.x);
            SetChildAlongAxis(item, 1, y_pos, cell_size.y);
        }
    }

    void instantiate_button(string name){
        UIButton prefab = Instantiate(button_prefab);
        prefab.transform.parent = this.transform;
        prefab.type = UIButton.Type.Button;
        prefab.name = name;
    }

    public void generate_from_json(){
        if(file_name == null){
            Debug.LogError("File missing, cant generate from JSON");
        }
        else{
            Games games_dict = JSONReader.load_json<Games>(file_name);
            foreach(Game game in games_dict.games){
                instantiate_button(game.name);
            }
        }
    }

	public void clear_all(){
        foreach (Transform child in transform) {
            GameObject.DestroyImmediate(child.gameObject);
        }
    }

    void Update(){
        if(!Application.isPlaying){
            do_the_stuff();
        }
    }

    void clamp_everything(){
        rows = Mathf.Clamp(rows, 1, 999);
        columns = Mathf.Clamp(columns, 1, 999);
        spacing_x = Mathf.Clamp(spacing_x, 0, 999);
        spacing_y = Mathf.Clamp(spacing_y, 0, 999);
        padding_left = Mathf.Clamp(padding.left, 0, 999);
        padding_right = Mathf.Clamp(padding.right, 0, 999);
        padding_top = Mathf.Clamp(padding.top, 0, 999);
        padding_bottom = Mathf.Clamp(padding.bottom, 0, 999);
        visable_buttons = Mathf.Clamp(visable_buttons, 1, 999);
    }


    public override void CalculateLayoutInputHorizontal(){
        base.CalculateLayoutInputHorizontal();
        clamp_everything();
        do_the_stuff();
    }

    public override void CalculateLayoutInputVertical(){}
    public override void SetLayoutHorizontal(){}
    public override void SetLayoutVertical(){}
}
