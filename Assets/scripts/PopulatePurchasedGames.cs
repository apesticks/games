﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopulatePurchasedGames : MonoBehaviour{

    public UIButton button_prefab;
	
    private List<string> button_names;

    void Start() {
        button_names = new List<string>();
        JSONReader.load_json<User>("user");
        update_purchased_list();
    }

    void instantiate_button(string name){
        UIButton prefab = Instantiate(button_prefab);
        prefab.transform.parent = transform;
        prefab.type = UIButton.Type.Tab;
        prefab.name = name;
        prefab.GetComponent<UIButton>().tab_group = this.GetComponent<UITabGroup>();
    }

    public void update_purchased_list(){
        User user = JSONReader.get_json<User>("user");
        foreach(Owned game in user.owned){
            if(!button_names.Contains(game.name)){
                instantiate_button(game.name);
                button_names.Add(game.name);
            }
        }
    }
}
