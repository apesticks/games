﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UITabGroup : MonoBehaviour {

    public Color enter_color;
    public Color pressed_color;
    public Color released_color;
    public UIButton button_selected;
    public UIButton button_prefab;

    private List<UIButton> buttons;
    private UIButton button_entered;
    private UIButton button_pressed;
    private bool button_exited = false;
    private bool done_once = false;


    public void subscribe(UIButton button){
        if(!done_once){
            buttons = new List<UIButton>();
            done_once = true;
        }
        //if(buttons == null){
            //buttons = new List<UIButton>();
        //}

        buttons.Add(button);
    }

    void Start(){
        if(button_selected != null){
            if(button_selected.type != UIButton.Type.Tab){
                Debug.LogError("TabGroup default button_selected: " + button_selected + " - is not a tab");
            }
            button_selected.image = button_selected.GetComponent<Image>();
            button_selected.image.color = button_selected.default_color;
            button_selected.selected = true;
            button_selected.set_color(released_color);
            button_selected.activate_page();
        }
    }

    public void on_button_enter(UIButton button){
        button_entered = button;
        button_exited = false;
        //reset_buttons();
        if(button_selected == null || button != button_selected){
            button.set_color(enter_color);
        }
    }

    public void on_button_exit(UIButton button){
        button_entered = null;
        button_exited = true;
        reset_buttons();
    }

    public void on_button_pressed(UIButton button){
        button_pressed = button;
        button.set_color(pressed_color);
    }

    public void on_button_released(UIButton button){
        if(!button_exited && button_entered == button_pressed){
            button_selected = button;
            button_selected.activate_page();
            button_selected.selected = true;
            button_selected.set_color(released_color);
            reset_buttons();
            reset_pages();

            button_selected._select();
        }
        button_pressed = null;
    }

    public void reset_pages(){
        foreach(UIButton button in buttons){
            if(button != button_selected && button.target_page != null){
                button.activate_page(false);
            }
        }
    }

    public void reset_buttons(){
        foreach(UIButton button in buttons){
            if(button != button_selected){
                button.set_color(button.default_color);
                button.selected = false;
            }
        }
    }
}
