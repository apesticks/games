﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActivatePurchaseButton : MonoBehaviour {

    public UITabGroup tab_group;
    public UIButton button;

    void Update(){
        if(tab_group.button_selected != null){
            button.gameObject.SetActive(true);
        }
        else{
            button.gameObject.SetActive(false);
        }
    }
}
