﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;


[RequireComponent(typeof(Image))]
[ExecuteInEditMode]
public class UIButton : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler, IPointerExitHandler, IPointerUpHandler{
    public enum Type { Button, Tab }
    
    public Type type = Type.Button;
    public UITabGroup tab_group;
    public GameObject target_page;

    public bool is_toggle;
    public bool disabled;
    public bool selected = false;
    public UnityEvent on_click;
    
    public Image image;
    public Color default_color;
    public Color enter_color;
    public Color pressed_color;
    public Color toggle_color;
    public Color disabled_color;

    private bool toggled = false;
    private bool exited = false;
    private bool set_disabled_color_once = false;

	void Update(){
        if(!Application.isPlaying){
            image.color = default_color;
        }
        if(disabled){
            set_color(disabled_color);
            set_disabled_color_once = false;
        }
        else if(!set_disabled_color_once && !selected){
            set_color(default_color);
            set_disabled_color_once = true;
        }
    }

    void Start(){
        image = GetComponent<Image>();
        if(disabled){
            set_color(disabled_color);
        }
        else{
            if(type == Type.Tab){ 
                if(tab_group == null){
                    Debug.LogError("Button: " + this + " is of type tab, but no tab_group assigned");
                }
                tab_group.subscribe(this); 
            }
        }
    }

    public void reset(){
        this.activate_page(false);
        this.set_color(default_color);
    }
    
    public void activate_page(bool _value=true){
        if(target_page != null){
            target_page.SetActive(_value);
        }
    }

    public void set_color(Color _color){
        if(image != null){
            image.color = _color;
        }
    }

    public void OnPointerEnter(PointerEventData eventData){
        if(disabled){
            set_color(disabled_color);
        }
        else{
            if(type == Type.Tab){ 
                tab_group.on_button_enter(this);
            }
            else{
                exited = false;
                set_color(enter_color);
            }
        }
    }

    public void OnPointerExit(PointerEventData eventData){
        if(disabled){
            set_color(disabled_color);
        }
        else{
            if(type == Type.Tab){ 
                tab_group.on_button_exit(this); 
            }
            else{
                exited = true;
                if(toggled){
                    set_color(toggle_color);
                }
                else{
                    set_color(default_color);
                }
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData){
        if(disabled){
            set_color(disabled_color);
        }
        else{
            if(type == Type.Tab){ 
                tab_group.on_button_pressed(this); 
            }
            else{
                set_color(pressed_color);
            }
        }
    }

    public void OnPointerUp(PointerEventData eventData){
        if(disabled){
            set_color(disabled_color);
        }
        else{
            if(type == Type.Tab){ 
                tab_group.on_button_released(this); 
            }
            else{
                if(!exited){
                    if(is_toggle){
                        toggled = !toggled;
                        if(toggled){
                            set_color(toggle_color);
                        }
                        else{
                            set_color(default_color);
                        }
                    }
                    else{
                        set_color(enter_color);
                    }

                    _select();
                }
            }
        }
    }

    public void _select(){
        if(on_click != null && !disabled){
            on_click.Invoke();
        }
    }

    public void quit(){
        Debug.Log("quit");
        Application.Quit();
    }
}
