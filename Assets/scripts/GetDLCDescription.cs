﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GetDLCDescription : MonoBehaviour{

    public UITabGroup tab_group;
    private TextMeshProUGUI TMP_text;
    private string selected_game;
    private bool found; 

    void Start(){
        JSONReader.load_json<Games>("games");
    }

    void Update(){
        TMP_text = this.GetComponent<TextMeshProUGUI>();
        if(tab_group.button_selected != null){
            found = false;
            Games games = JSONReader.get_json<Games>("games");

            foreach(Game game in games.games){
                foreach(DLC dlc in game.dlc){
                    if(dlc.name == tab_group.button_selected.name){
                        found = true;
                        if(selected_game != dlc.name){
                            selected_game = dlc.name;
                            TMP_text.text = dlc.description;
                        }
                    }
                }
            }
        }
        else{
            TMP_text.text = "";
        }
    }
}
