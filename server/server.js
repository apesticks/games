const uuidv4  = require('uuidv4');
const http = require('http');
const url = require('url');
const WebSocket = require('ws');

const PORT = 3000;
const server = http.createServer();

//let reversed = a =>[...a].map(a.pop,a);

class SocketGroup {
  constructor() {
    this.connections = {};
    console.log('Creating playerStateActions');
    this.playerStateActions = [];
  }

  onlyHostConnected() {
    // TODO: If no host is joined, set the new host
    return Object.keys(this.connections).length == 1
  }

  getHostUUID() {
    let latestUUID;
    for (const actionIdx in this.playerStateActions) {
      const action = this.playerStateActions[actionIdx];
      if (action.method == 'setHost') {
        latestUUID = action.uuid;
      }
    }
    return latestUUID;
  }

  addConnection(uuid, ws) {
    const addPayload = {
      method: 'addPlayer',
      uuid: uuid,
      nickname: '',
      host: false,
    };
    this.connections[uuid] = ws;

    // If a new connection is added, unify the new player's state by executing
    // all stored actions in order. This will ensure that their lobby state is
    // syncronized.
    //
    // Note: This is only necessary to be done from here (addConnection). All
    // actions after that are handled via sendToAll.
    this.synchronizeNewPlayer(uuid);
    this.sendToAll(addPayload);

    // Assume for the moment that the first player is the host.
    // TODO: Handle host assignment independently from lobby
    if (this.onlyHostConnected()) {
      this.setHostMethod(uuid, uuid);
    }
  }

  setHostMethod(requesterUUID, designeeUUID) {
    // Only the host can change the host, bail otherwise.
    // On the first run, ignore this rule.
    if (!this.onlyHostConnected() && this.getHostUUID() != requesterUUID) {
      return;
    }

    console.log(`[DEBUG] Player ${requesterUUID} setting host to ${designeeUUID}`);
    const setHostPayload = {
      method: 'setHost',
      uuid: designeeUUID,
    };
    this.sendToAll(setHostPayload);
  }

  changeNicknameMethod(uuid, inputPayload) {
    // TODO: Change generic "text" key to something else?
    const nickname = inputPayload.text;
    const changeNicknamePayload = {
      method: 'changeNickname',
      uuid: uuid,
      nickname: nickname,
    };
    this.sendToAll(changeNicknamePayload);
  }

  toggleReadyMethod(uuid, payload) {
    console.log("[DEBUG] Toggling ready status for " + uuid);
    const ws = this.connections[uuid];
    //const latestAction = this.getLatestAction(uuid, 'toggleReady');

    const toggleReadyPayload = {
      method: 'toggleReady',
      uuid: uuid,
    };
    this.sendToAll(toggleReadyPayload);
  }

  synchronizeNewPlayer(uuid) {
    // TODO: Bundle together actions rather than sending one message per action
    // TODO: Cull stale actions
    for (const actionIdx in this.playerStateActions) {
      const action = this.playerStateActions[actionIdx];
      this.sendByUUID(uuid, action);
    }
  }

  listActions(method) {
    let actionsOut = [];

    for (const actionIdx in this.playerStateActions) {
      const action = this.playerStateActions[actionIdx];
      if (action.method == method) {
        actionsOut.push(action);
      }
    }
    return actionsOut;
  }

  getLatestAction(method) {
    let lastAction = null;

    for (const idx in this.playerStateActions) {
      const action = this.playerStateActions[idx];
      if (action.method == method) {
        lastAction = action;
        //return action
      }
    }
    return lastAction;
  }

  applyAction(payload) {
    this.playerStateActions.push(payload);
  }

  getNextHost(currentUUID) {
    const actions = this.listActions('addPlayer');
    for (const actionIdx in actions) {
      const action = actions[actionIdx];
      if (action.uuid == currentUUID) {
        continue;
      }

      const playerIsConnected = Object.keys(this.connections).indexOf(action.uuid);
      console.log("----");
      console.log(action);
      console.log(playerIsConnected);
      if (playerIsConnected) {
        return action.uuid;
      }
    }
  }

  playerIsHost(uuid) {
    const latestHost = this.getLatestAction('setHost');
    if (latestHost) {
      if (latestHost.uuid == uuid) {
        return true;
      }
    }
    return false;
  }

  closeConnection(uuid) {
    let removePayload = {
      method: 'removePlayer',
      uuid: uuid,
    };

    // If the host leaves, we need to designate a new host
    // on their behalf. Use the next player that connected.
    if (this.playerIsHost(uuid)) {
      console.log('%%%%');
      console.log(this.playerStateActions);
      const nextHostUUID = this.getNextHost(uuid);

      console.log('^^^^');
      console.log(this.playerStateActions);
      if (nextHostUUID) {
        this.setHostMethod(uuid, nextHostUUID);
      } else {
        console.log('[DEBUG] No host available!');
      }
    }

    //delete this.connections[uuid];
    this.applyAction(removePayload);
    this.sendToAll(removePayload);
  }

  getWs(uuid) {
    return this.connections[uuid];
  }

  sendByUUID(uuid, payload) {
    const serializedPayload = JSON.stringify(payload);
    console.log(`[DEBUG] Sending payload ${serializedPayload} to ${uuid}`);
    this.getWs(uuid).send(serializedPayload);
  }

  sendToAll(payload) {
    // TODO: Global prune
    this.applyAction(payload);
    for (const uuid in this.connections) {
      this.sendByUUID(uuid, payload);
    }
  }

  getMethod(methodName) {
    console.log("[DEBUG] Looking up method name " + methodName);
    return this[methodName + "Method"];
  }
}

// TODO: Add distinction between host and non-host, add startup method for non-host clients.
// TODO: Properly handle non-graceful disconnections
class HostedGame {
  constructor(socket) {
    this.connections = new SocketGroup();
  }

  addConnection(ws, uuidPlayer) {
    console.log(`[DEBUG] Adding connection: ${uuidPlayer}`);
    this.connections.addConnection(uuidPlayer, ws);
  }

  closeConnection(uuidPlayer) {
    console.log(`[DEBUG] Closing connection: ${uuidPlayer}`);
    this.connections.closeConnection(uuidPlayer);
  }

  routeAction(uuid, method, payload) {
    // TODO: Securitah: let's not let an attacker run arbitrary functions, yeah?
    const methodFn = this.connections.getMethod(method);

    // Acceptable method names:
    // changeNicknameMethod
    // toggleReadyMethod

    // TODO: Must we really pass in an empty payload even if one is not needed?
    if (methodFn != undefined) {
      console.log('[DEBUG] Routing action for ' + uuid +': ' + method + '(' + payload + ')');
      methodFn.apply(this.connections, [uuid, payload]);
    } else {
      console.log('[DEBUG] Invalid command requested');
    }
  }
}


wss = new WebSocket.Server({
	noServer: true,
	clientTracking: true
});

// TODO: Create a new game session per hosted game instead of a single global hosted game
var hostedGame = new HostedGame();

wss.on('connection', function connection(ws, request) {
  var uuidPlayer = uuidv4.uuid();

  //console.log(new Date() + ` | A new client is connected ${uuidPlayer}.`);

  // Registering player with the session.
  let registerMessage = {
    type: 'register',
    method: 'register',
    sessionId: request.headers['sec-websocket-key'],
    uuidPlayer: uuidPlayer,
  };

  // Register the player by sending back the session message
  ws.send(JSON.stringify(registerMessage));
  hostedGame.addConnection(ws, uuidPlayer);

  ws.on('message', function(msgStr) {
    // TODO: Do we really need to double-encode??
    const outerPayload = JSON.parse(msgStr);
    let innerPayload = {};

    if (outerPayload.message) {
      innerPayload = JSON.parse(outerPayload.message);
    }
    const {method} = outerPayload;

    if (method != undefined) {
      hostedGame.routeAction(uuidPlayer, method, innerPayload);
    } else {
      console.log("[DEBUG] ERROR: No method specified for message");
    }
  });

  ws.on('disconnect', function(connection) {
  });

  ws.on('close', function(connection) {
    // TODO: Figure out if disconnect is needed
    // TODO: Instrument closing a session
    hostedGame.closeConnection(uuidPlayer);
  });
});

// HTTP Server ==> WebSocket upgrade handling:
server.on('upgrade', function upgrade(request, socket, head) {

    //console.log(new Date() + ' | Upgrading http connection to wss: url = '+request.url);

    // Parsing url from the request.
    var parsedUrl = url.parse(request.url, true, true);
    const pathname = parsedUrl.pathname

    // console.log(new Date() + ' | Pathname = '+pathname);

    // If path is valid connect to the websocket.
    if (pathname === '/') {
      wss.handleUpgrade(request, socket, head, function done(ws) {
        wss.emit('connection', ws, request);
      });
    } else {
      socket.destroy();
    }
});

// On establishing PORT listener.
server.listen(PORT, function() {
    console.log(new Date() + ' | Server is listening on port ' + PORT);

    // Server is running.
});
